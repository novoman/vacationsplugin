package com.plansource.beltech.VacationsPlugin.resource;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.util.UserManager;
import com.plansource.beltech.VacationsPlugin.EmailNotificator;
import com.plansource.beltech.VacationsPlugin.VacationsHelper;
import com.plansource.beltech.VacationsPlugin.ao.UserRequests;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Path("/vacations")
public class VacationsResource
{
	private ActiveObjects activeObjects;
	private VacationsHelper vacationsHelper;
	private EmailNotificator emailNotificator;

	public VacationsResource(ActiveObjects activeObjects, UserManager userManager)
	{
		this.activeObjects = activeObjects;
		this.vacationsHelper = new VacationsHelper(this.activeObjects, userManager);
		this.emailNotificator = new EmailNotificator(this.activeObjects, userManager);
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/addUserRequest")
	public Response addUserRequest(
		@Context HttpServletRequest request,
		@FormParam("request-type") String request_type,
		@FormParam("start-date") String start_date,
		@FormParam("end-date") String end_date,
		@FormParam("description") String description
	)
	{
		Long userId = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getId();
		boolean canAutoApprove = VacationsHelper.canAutoApprove();

		if (userId != null) {
			UserRequests userRequest = this.activeObjects.create(UserRequests.class);
			userRequest.setRequestById(userId);

			if (!this.vacationsHelper.isValidRequestType(request_type)) {
				return Response.serverError().entity("Invalid user request types.").build();
			}

			userRequest.setRequestType(UserRequests.RequestTypes.valueOf(request_type));

			if (!description.isEmpty()) {
				userRequest.setDescription(description);
			}

			try {
				Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(start_date);
				Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(end_date);
				Date date = new Date();

				userRequest.setStartDate(startDate);
				userRequest.setEndDate(endDate);
				userRequest.setCreatedAt(date);
				userRequest.setUpdatedAt(date);
			} catch (ParseException e) {
				return Response.serverError().entity("Can't parse user request dates.").build();
			}

			if (this.vacationsHelper.isValidDatesForRequest(userRequest)) {
				userRequest.save();
			}
			else {
				this.activeObjects.delete(userRequest);
				return Response.serverError().entity("Invalid user request dates.").build();
			}

			if (canAutoApprove) {
				if (approveRequest(userRequest, "")) {
					if (this.emailNotificator.sendAutoApprovedNotificationEmail(userRequest, "")) {
						Response.ok().entity("The request was successfully created.").build();
					}
					else {
						return Response.serverError().entity("Request was auto approved, but a notification email wasn't sent.").build();
					}
				}
				else {
					Response.serverError().entity("Error with auto approving the request: " +  this.vacationsHelper.requestToString(userRequest, "without_approve_inf")).build();
				}
			}
			else {
				if (!this.emailNotificator.sendRequestedNotificationEmail(userRequest)) {
					return Response.serverError().entity("Request was created, but a notification email wasn't sent.").build();
				}
			}
		}

		return Response.ok().entity("The request was successfully created.").build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/deleteUserRequest")
	public Response deleteUserRequest(@Context HttpServletRequest request)
	{
		int requestId = Integer.parseInt(request.getParameter("request-id"));
		UserRequests userRequest = this.activeObjects.get(UserRequests.class, requestId);
		Long userId = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getId();

		boolean canAutoApprove = VacationsHelper.canAutoApprove();

		if (userRequest != null && userRequest.getRequestById().equals(userId)) {
			if (userRequest.getApprovedById() == null) {
				this.activeObjects.delete(userRequest);
			}
			else {
				UserRequests[] submittedCancelledRequests = this.activeObjects.find(UserRequests.class, "CANCELLED_USER_REQUEST_ID = ? AND APPROVE_RESULT IS NULL",  userRequest.getID());
				if (submittedCancelledRequests.length > 0) {
					return Response.serverError().entity("Request for cancel '" + this.vacationsHelper.requestToString(userRequest, "without_approve_inf") + "' was already created. Waiting for PM approval.").build();
				}
				UserRequests cancelledUserRequest = this.activeObjects.create(UserRequests.class);
				cancelledUserRequest.setCancelledUserRequestId((long) userRequest.getID());
				cancelledUserRequest.setRequestType(UserRequests.RequestTypes.cancel);
				cancelledUserRequest.setStartDate(userRequest.getStartDate());
				cancelledUserRequest.setEndDate(userRequest.getEndDate());
				cancelledUserRequest.setRequestById(userId);
				Date date = new Date();
				cancelledUserRequest.setCreatedAt(date);
				cancelledUserRequest.setUpdatedAt(date);
				cancelledUserRequest.save();

				if (canAutoApprove) {
					String deletedRequestToString = vacationsHelper.requestToString(cancelledUserRequest, "");
					if (approveRequest(cancelledUserRequest, "")) {
						if (this.emailNotificator.sendAutoApprovedNotificationEmail(cancelledUserRequest, deletedRequestToString)) {
							Response.ok().entity("The request was successfully deleted.").build();
						}
						else {
							return Response.serverError().entity("Request was auto approved, but a notification email wasn't sent.").build();
						}
					}
					else {
						Response.serverError().entity("Error with auto approving the request: " +  this.vacationsHelper.requestToString(cancelledUserRequest, "without_approve_inf")).build();
					}
				}
				else {
					if (!this.emailNotificator.sendRequestedNotificationEmail(cancelledUserRequest)) {
						return Response.serverError().entity("Request was created, but a notification email wasn't sent.").build();
					}
					else {
						return Response.ok().entity("Request for cancel '" + vacationsHelper.requestToString(userRequest, "without_approve_inf") + "' was successfully created.").build();
					}
				}
			}
		}

		return Response.ok().entity("The request was successfully deleted.").build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/approveUserRequest")
	public Response approveUserRequest(@Context HttpServletRequest request)
	{
		int requestId = Integer.parseInt(request.getParameter("request-id"));
		String pmDescription = request.getParameter("pm-description");
		UserRequests[] userRequests = this.activeObjects.find(UserRequests.class, "ID = ? AND APPROVED_BY_ID IS NULL", requestId);

		if (userRequests.length > 0) {
			UserRequests userRequest = userRequests[0];
			String deletedRequestToString = vacationsHelper.requestToString(userRequest, "");
			if (approveRequest(userRequest, pmDescription)) {
				if (!this.emailNotificator.sendProcessedNotificationEmail(userRequest, deletedRequestToString)) {
					return Response.serverError().entity("Request was approved, but a notification email wasn't sent.").build();
				}
				return Response.ok().entity("The request was successfully approved.").build();
			}
		}

		return Response.serverError().entity("Error with approving the request.").build();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Path("/declineUserRequest")
	public Response declineUserRequest(@Context HttpServletRequest request)
	{
		int requestId = Integer.parseInt(request.getParameter("request-id"));
		UserRequests[] userRequests = this.activeObjects.find(UserRequests.class, "ID = ? AND APPROVED_BY_ID IS NULL", requestId);

		if (userRequests.length > 0) {
			UserRequests userRequest = userRequests[0];
			if (userRequest != null) {
				Long userId = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getId();
				userRequest.setApprovedById(userId);
				userRequest.setApproveResult(UserRequests.RequestResults.declined);
				String pmDescription = request.getParameter("pm-description");
				if (!pmDescription.isEmpty()) {
					userRequest.setPmDescription(pmDescription);
				}
				Date date = new Date();
				userRequest.setUpdatedAt(date);
				userRequest.save();

				if (!this.emailNotificator.sendProcessedNotificationEmail(userRequest, "")) {
					return Response.serverError().entity("Request was declined, but a notification email wasn't sent.").build();
				}
			}
		}

		return Response.ok().entity("The request was successfully declined.").build();
	}

	private boolean approveRequest(UserRequests userRequest, String pmDescription) {
		Long userId = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getId();

		if (userRequest.getRequestType().equals(UserRequests.RequestTypes.cancel)) {
			userRequest.setApprovedById(userId);
			UserRequests[] requestsToCancel = this.activeObjects.find(UserRequests.class, "ID = ?",  userRequest.getCancelledUserRequestId());

			if (requestsToCancel.length > 0) {
				UserRequests requestToCancel = requestsToCancel[0];
				if (requestToCancel != null) {
					UserRequests[] cancelledUserRequests = this.activeObjects.find(UserRequests.class, "CANCELLED_USER_REQUEST_ID = ?", requestToCancel.getID());

					this.activeObjects.delete(cancelledUserRequests);
					this.activeObjects.delete(requestToCancel);
				}
			}
		}
		else {
			userRequest.setApprovedById(userId);
			userRequest.setApproveResult(UserRequests.RequestResults.approved);
			if (!pmDescription.isEmpty()) {
				userRequest.setPmDescription(pmDescription);
			}
			Date date = new Date();
			userRequest.setUpdatedAt(date);
			userRequest.save();
		}

		return true;
	}
}
