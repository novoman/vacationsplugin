package com.plansource.beltech.VacationsPlugin.servlet;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.VacationsPlugin.VacationsHelper;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Scanned
public class VacationsCalendarServlet extends HttpServlet
{
    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public VacationsCalendarServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (VacationsHelper.getCurrentUserRole().isEmpty()) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        String paramsUserId = request.getParameter("userId");
        Map<String, Object> context = Maps.newHashMap();

        VacationsHelper vacationsHelper = new VacationsHelper(this.activeObjects, this.userManager);
        context.put("paramsUserId", paramsUserId);
        context.put("vacationsHelper", vacationsHelper);

        this.webResourceManager.requireResource("com.plansource.beltech.VacationsPlugin:vacations-resources");
        String content = this.velocityManager.getEncodedBody("/templates/beltech/", "calendar.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }
}
