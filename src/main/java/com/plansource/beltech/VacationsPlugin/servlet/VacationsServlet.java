package com.plansource.beltech.VacationsPlugin.servlet;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.webresource.WebResourceManager;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.plansource.beltech.VacationsPlugin.VacationsHelper;
import com.atlassian.velocity.VelocityManager;
import com.google.common.collect.Maps;
import com.plansource.beltech.VacationsPlugin.ao.UserRequests;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Scanned
public class VacationsServlet extends HttpServlet
{
    private VelocityManager velocityManager;
    private ActiveObjects activeObjects;
    private WebResourceManager webResourceManager;
    private UserManager userManager;

    public VacationsServlet(
        @ComponentImport VelocityManager velocityManager,
        @ComponentImport ActiveObjects activeObjects,
        @ComponentImport WebResourceManager webResourceManager,
        @ComponentImport UserManager userManager
    )
    {
        this.velocityManager = velocityManager;
        this.activeObjects = activeObjects;
        this.webResourceManager = webResourceManager;
        this.userManager = userManager;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        if (VacationsHelper.getCurrentUserRole().isEmpty()) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }

        Map<String, Object> context = Maps.newHashMap();

        if (currentUser != null) {
            boolean isBeltechPm = ComponentAccessor.getGroupManager().isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("pm"));

            VacationsHelper vacationsHelper = new VacationsHelper(this.activeObjects, this.userManager);
            UserRequests[] submittedRequests = vacationsHelper.getUserRequestsForMyRequests(currentUser, "submitted", false);
            UserRequests[] approvedRequests = vacationsHelper.getUserRequestsForMyRequests(currentUser, "approved", false);
            UserRequests[] declinedRequests = vacationsHelper.getUserRequestsForMyRequests(currentUser, "declined", false);

            context.put("userManager", this.userManager);
            context.put("vacationsHelper", vacationsHelper);
            context.put("isBeltechPm", isBeltechPm);
            context.put("paramsUserId", "all");
            context.put("submittedRequests", submittedRequests);
            context.put("submittedRequestsLength", submittedRequests.length);
            context.put("approvedRequests", approvedRequests);
            context.put("approvedRequestsLength", approvedRequests.length);
            context.put("declinedRequests", declinedRequests);
            context.put("declinedRequestsLength", declinedRequests.length);

            if (isBeltechPm) {
                UserRequests[] requestsToApprove = vacationsHelper.getUserRequestsForMyRequests(currentUser, "submitted", true);
                context.put("requestsToApprove", requestsToApprove);
                context.put("requestsToApproveLength", requestsToApprove.length);
            }

        }

        this.webResourceManager.requireResource("com.plansource.beltech.VacationsPlugin:vacations-resources");
        String content = this.velocityManager.getEncodedBody("/templates/", "index.vm", "UTF-8", context);
        response.setContentType("text/html;charset=utf-8");
        response.getWriter().write(content);
        response.getWriter().close();
    }
}
