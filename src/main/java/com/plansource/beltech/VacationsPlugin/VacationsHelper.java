package com.plansource.beltech.VacationsPlugin;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.plansource.beltech.VacationsPlugin.ao.UserRequests;
import net.java.ao.Query;
import org.apache.commons.lang3.ArrayUtils;

public class VacationsHelper {

    private ActiveObjects activeObjects;
    private UserManager userManager;

    public static final Map<String, String> beltechUserRoles = new HashMap<String, String>()
    {
        {
            put("dev", "BelTech Belarus IPUs");
            put("pm", "BelTech SLC");
            put("ch", "BelTech Belarus CH");
            put("qa", "BelTech QA");
        };
    };

    public VacationsHelper(ActiveObjects activeObjects, UserManager userManager) {
        this.activeObjects = activeObjects;
        this.userManager = userManager;
    }

    public boolean isValidRequestType(String type)
    {
        for (UserRequests.RequestTypes rt : UserRequests.RequestTypes.values()) {
            if (rt.name().equals(type)) {
                return true;
            }
        }

        return false;
    }

    public boolean isValidDatesForRequest(UserRequests request)
    {
        if (request.getStartDate().after(request.getEndDate()) && !request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
            return false;
        }
        else if (request.getEndDate().after(request.getStartDate()) && request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
            return false;
        }

        return true;
    }

    public String getMonthName(int monthNumber) {
        String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        return months[monthNumber - 1];
    }

    public int getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public String getIntersectionCalendarCellClasses(String dateString, String paramsUserId)
    {
        String classes = "";
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateString);

            if (date != null) {
                classes += "cell";
                SimpleDateFormat simpleDateformat = new SimpleDateFormat("EEEE");
                if (simpleDateformat.format(date).equals("Saturday") || simpleDateformat.format(date).equals("Sunday")) {
                    classes += " weekend";
                }
                else {
                    int summaryLevelNumber = calculateIntersectionsSummary(date, paramsUserId);
                    if (summaryLevelNumber > 9) summaryLevelNumber = 9;

                    classes += " level-" + summaryLevelNumber;
                    Date today = sdf.parse(sdf.format(new Date()));

                    if (date.equals(today)) {
                        classes += " today";
                    }
                    else if (date.before(today)) {
                        classes += " past";
                    }
                }
            }
            else {
                classes = "date-invalid";
            }
        } catch (ParseException e) {
            return "date-invalid";
        }

        return classes;
    }

    private int calculateIntersectionsSummary(Date date, String paramsUserId) {
        UserRequests[] userRequests = findUserRequestsForCalendar(date, paramsUserId);

        return userRequests.length;
    }

    public String getProgrammersList(String dateString, String paramsUserId)
    {
        StringBuilder result = new StringBuilder();
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date date = sdf.parse(dateString);

            if (date != null) {
                UserRequests[] userRequests = findUserRequestsForCalendar(date, paramsUserId);

                for (UserRequests userRequest : userRequests) {
                    if (this.userManager.getUserById(userRequest.getRequestById()).isPresent()) {
                        result.append(this.userManager.getUserById(userRequest.getRequestById()).get().getUsername()).append("\n");
                    }
                }

                result.append("----------\nTOTAL ").append(userRequests.length);
            }
        }
        catch (ParseException e) {
            return "Parse error";
        }

        return result.toString();
    }

    private UserRequests[] findUserRequestsForCalendar(Date date, String paramsUserId) {
        UserRequests[] tempUserRequests, userRequests = new UserRequests[0];
        List<ApplicationUser> users =  selectForUsersVacation("all", paramsUserId);

        for (ApplicationUser user : users) {
            tempUserRequests = this.activeObjects.find(UserRequests.class, "START_DATE  <= ? AND END_DATE >= ? AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", date, date, UserRequests.RequestResults.approved, user.getId());
            userRequests = ArrayUtils.addAll(userRequests, tempUserRequests);

            tempUserRequests = this.activeObjects.find(UserRequests.class, "START_DATE  = ? AND REQUEST_TYPE = ? AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", date, UserRequests.RequestTypes.working_day_transfer, UserRequests.RequestResults.approved, user.getId());
            userRequests = ArrayUtils.addAll(userRequests, tempUserRequests);
        }

        return userRequests;
    }

    public String beltechPmEmails()
    {
        StringBuilder result = new StringBuilder();
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        List<ApplicationUser> users = new ArrayList<>(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("pm")));

        for (ApplicationUser user : users) {
            result.append(user.getEmailAddress()).append(", ");
        }

        return result.substring(0, result.length() - 2);
    }

    public List<ApplicationUser> selectForUsersVacation(String typeForPm, String paramsUserId) {
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        List<ApplicationUser> applicationUsers = new ArrayList<>();
        String userRole = getCurrentUserRole();

        if (!paramsUserId.equals("all")) {
            Long requestId = Long.parseLong(paramsUserId);
            if (this.userManager.getUserById(requestId).isPresent()) {
                applicationUsers.add(this.userManager.getUserById(requestId).get());
                return applicationUsers;
            }
        }

        switch (userRole) {
            case "PM":
                switch (typeForPm) {
                    case "all":
                        applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("pm")));
                        applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("dev")));
                        break;
                    case "pms":
                        applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("pm")));
                        break;
                    case "devs":
                        applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("dev")));
                        break;
                }
                break;
            case "CH":
                applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("ch")));
                break;
            case "QA":
                applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("qa")));
                break;
            case "DEV":
                applicationUsers.addAll(groupManager.getUsersInGroup(VacationsHelper.beltechUserRoles.get("dev")));
                break;
        }

        return applicationUsers;
    }

    public static String getCurrentUserRole() {
        String userRole = "";

        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        GroupManager groupManager = ComponentAccessor.getGroupManager();

        if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("pm")))  userRole = "PM";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("ch"))) userRole = "CH";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("qa"))) userRole = "QA";
        else if (groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("dev"))) userRole = "DEV";

        return userRole;
    }

    public UserRequests[] getUserRequestsForMyRequests(ApplicationUser user, String type, boolean isBeltechPm)
    {
        Query query = Query.select();

        if ("submitted".equals(type)) {
            if (isBeltechPm) {
                query = query.where("APPROVE_RESULT IS NULL");
            }
            else {
                query = query.where("REQUEST_BY_ID = ? AND APPROVE_RESULT IS NULL", user.getId());
            }

        }
        else {
            query = query.where("REQUEST_BY_ID = ? AND APPROVE_RESULT = '" + type + "'", user.getId());
        }

        return this.activeObjects.find(UserRequests.class, query);
    }

    public UserRequests[] getUserRequestsForProcessedVacations(ApplicationUser user, int year) {
        UserRequests[] userRequests = new UserRequests[0];
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            Date yearStart = sdf.parse(year + "-01-01");
            Date yearEnd = sdf.parse(year + "-12-31");

            if (yearStart != null && yearEnd != null) {
                UserRequests[] approvedProcessedRequests, declinedProcessedRequests;

                approvedProcessedRequests = this.activeObjects.find(UserRequests.class, "((START_DATE  <= ? AND START_DATE >= ?) OR (END_DATE >= ? AND END_DATE <= ?)) AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", yearStart, yearEnd, yearStart, yearEnd, UserRequests.RequestResults.approved, user.getId());
                declinedProcessedRequests = this.activeObjects.find(UserRequests.class, "((START_DATE  <= ? AND START_DATE >= ?) OR (END_DATE >= ? AND END_DATE <= ?)) AND APPROVE_RESULT = ? AND REQUEST_BY_ID = ?", yearStart, yearEnd, yearStart, yearEnd, UserRequests.RequestResults.declined, user.getId());
                userRequests = ArrayUtils.addAll(approvedProcessedRequests, declinedProcessedRequests);
            }
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return userRequests;
    }

    public static boolean canAutoApprove() {
        GroupManager groupManager = ComponentAccessor.getGroupManager();
        ApplicationUser currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        return groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("pm")) ||
               groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("ch")) ||
               groupManager.isUserInGroup(currentUser, VacationsHelper.beltechUserRoles.get("qa"));
    }

    public String requestToString(UserRequests request, String approvedBy)
    {
        String result = "";

        switch (request.getRequestType()) {
            case working_day_transfer:
            case day_off:
                result += "Day off";
                break;
            case cancel:
                result += "Cancel";
                break;
            case sick_day:
                result += "Sick day";
                break;
            case vacation:
                result += "Vacation";
                break;
        }

        if (request.getRequestType().equals(UserRequests.RequestTypes.cancel)) {
            UserRequests[] requestsToCancel = this.activeObjects.find(UserRequests.class, "ID = ?",  request.getCancelledUserRequestId());

            if (requestsToCancel.length > 0) {
                UserRequests requestToCancel = requestsToCancel[0];
                if (requestToCancel != null) {
                    result += " '" + this.requestToString(requestToCancel, "without_approve_inf") + "'";
                }
            }
        }
        else {
            if (request.getRequestType().equals(UserRequests.RequestTypes.working_day_transfer)) {
                result += " on " + request.getStartDate().toString().substring(0, 10) + ". I can work instead on " + request.getEndDate().toString().substring(0, 10);
            }
            else {
                if (request.getStartDate().equals(request.getEndDate())) {
                    result +=  " on " + request.getStartDate().toString().substring(0, 10);
                }
                else if ((request.getStartDate() != null) && (request.getEndDate() != null) && (request.getStartDate() != request.getEndDate())) {
                    result +=  " from " + request.getStartDate().toString().substring(0, 10) + " to " + request.getEndDate().toString().substring(0, 10);
                }
            }

            if (!approvedBy.equals("without_approve_inf")) {
                if (request.getRequestById().equals(request.getApprovedById())) {
                    result +=  " (auto approved)";
                }
                else {
                    if (request.getApproveResult() != null) {
                        result +=  " was " + request.getApproveResult().toString();
                    }
                    if ((request.getApproveResult() != null) && (!approvedBy.isEmpty())) {
                        result +=  " by " + approvedBy;
                    }
                }
            }
        }

        if (request.getDescription() != null) {
            result +=  " (" + request.getDescription() + ")";
        }
        if (request.getPmDescription() != null) {
            result +=  " PM: " + request.getPmDescription();
        }

        return result;
    }
}
