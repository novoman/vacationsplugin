package com.plansource.beltech.VacationsPlugin.ao;

import net.java.ao.Entity;
import net.java.ao.OneToOne;
import net.java.ao.schema.Default;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Unique;

import java.util.Date;

public interface UserRequests extends Entity
{
	enum RequestTypes
	{
		vacation,
		day_off,
		working_day_transfer,
		cancel,
		sick_day
	}

	enum RequestResults
	{
		approved,
		declined
	}

	RequestTypes getRequestType();
	void setRequestType(RequestTypes type);

	Date getStartDate();
	void setStartDate(Date date);

	Date getEndDate();
	void setEndDate(Date date);

	Long getRequestById();
	void setRequestById(Long userId);

	Long getApprovedById();
	void setApprovedById(Long userId);

	Long getCancelledUserRequestId();
	void setCancelledUserRequestId(Long requestId);

	RequestResults getApproveResult();
	void setApproveResult(RequestResults type);

	@StringLength(StringLength.UNLIMITED)
	String getDescription();
	void setDescription(String description);

	Date getCreatedAt();
	void setCreatedAt(Date date);

	Date getUpdatedAt();
	void setUpdatedAt(Date date);

	@StringLength(StringLength.UNLIMITED)
	String getPmDescription();
	void setPmDescription(String pmDescription);
}
