package com.plansource.beltech.VacationsPlugin;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.PluginParseException;

import java.util.Map;

public class UserInGroupCondition extends AbstractWebCondition {
    private String groupName;

    public UserInGroupCondition() {
    }

    public void init(Map<String,String> params) throws PluginParseException
    {
        this.groupName = params.get("groupName");
        super.init(params);
    }

    public boolean shouldDisplay(ApplicationUser user, JiraHelper jiraHelper) {
        return ComponentAccessor.getGroupManager().isUserInGroup(user, this.groupName);
    }
}
