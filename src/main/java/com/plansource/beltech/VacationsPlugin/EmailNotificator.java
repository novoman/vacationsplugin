package com.plansource.beltech.VacationsPlugin;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.mail.Email;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.mail.queue.SingleMailQueueItem;
import com.plansource.beltech.VacationsPlugin.ao.UserRequests;

import java.util.HashMap;
import java.util.Map;

public class EmailNotificator
{
    private UserManager userManager;
    private VacationsHelper vacationsHelper;

    private static final String beltechVacationPath = "https://jira.plansource.com/secure/Vacations.jspa";

    private static final Map<String, String> beltechEmails = new HashMap<String, String>()
    {
        {
//        put("BELTECH_PM", "enaruta@beltechsoftware.com, mshuleyko@beltechsoftware.com, smazur@beltechsoftware.com, dkadyrko@plansource.com, akushnir@plansource.com");
          put("BELTECH_BOSS", "igor.motro@834andmore.com, vtabachenko@beltechsoftware.com");
        };
    };

    public EmailNotificator(ActiveObjects activeObjects, UserManager userManager) {
        this.userManager = userManager;
        this.vacationsHelper = new VacationsHelper(activeObjects, this.userManager);
        beltechEmails.put("BELTECH_PM", this.vacationsHelper.beltechPmEmails());
    }

    public boolean sendRequestedNotificationEmail(UserRequests request)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from, cc, userName, subject, take, message;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            to = beltechEmails.get("BELTECH_PM");

            from = user.getEmailAddress().isEmpty() ? "jira@plansource.com" : user.getEmailAddress();
            cc = from;
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();
            subject = "Request for " + this.vacationsHelper.requestToString(request, "without_approve_inf") + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            message = "Hello BelTech PMs,\nI would like to" + take + " " + this.vacationsHelper.requestToString(request, "without_approve_inf") + ". ";
            message += "Please approve " + beltechVacationPath + "\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, cc, subject, message);
        }

        return false;
    }

    public boolean sendProcessedNotificationEmail(UserRequests request, String deletedRequestToString)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from, cc, userName, processedBy, processedResult, subject, take, message, requestToString;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            to = beltechEmails.get("BELTECH_PM") + ", " + beltechEmails.get("BELTECH_BOSS");

            from = user.getEmailAddress().isEmpty() ? "jira@plansource.com" : user.getEmailAddress();
            cc = from;
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();
            processedResult = (request.getApproveResult() == null) || request.getApproveResult().equals(UserRequests.RequestResults.approved) ? "Approve" : "Decline";
            requestToString = deletedRequestToString.isEmpty() ? this.vacationsHelper.requestToString(request, "without_approve_inf") : deletedRequestToString;

            subject = processedResult + " for " + requestToString + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            if ((request.getApproveResult() == null) || request.getApproveResult().equals(UserRequests.RequestResults.approved)) {
                message = "Hi, " + userName + ",\n\nWe are Ok with this. User event was added to your account.\n\n";
            }
            else {
                message = "Hi, " + userName + ",\n\nRequest declined.\n\n";
            }

            if (request.getPmDescription() != null) {
                message += request.getPmDescription() + "\n\n";
            }

            processedBy = "";

            if (this.userManager.getUserById(request.getApprovedById()).isPresent()) {
                if (!this.userManager.getUserById(request.getApprovedById()).get().getUsername().isEmpty()) {
                    processedBy = this.userManager.getUserById(request.getApprovedById()).get().getUsername();
                }
            }
            message += "Thank you, \n" + processedBy + "\n\n\n";

            message += "Hello BelTech PMs,\nI would like to" + take + " " + requestToString + ". ";
            message += "Please approve " + beltechVacationPath + "\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, cc, subject, message);
        }

        return false;
    }

    public boolean sendAutoApprovedNotificationEmail(UserRequests request, String deletedRequestToString)
    {
        if (request.getRequestById() == null) {
            return false;
        }

        String to, from, cc, userName, subject, take, message, requestToString;

        if (this.userManager.getUserById(request.getRequestById()).isPresent()) {
            ApplicationUser user = this.userManager.getUserById(request.getRequestById()).get();

            to = beltechEmails.get("BELTECH_BOSS");
            from = user.getEmailAddress().isEmpty() ? "jira@plansource.com" : user.getEmailAddress();
            cc = from;
            userName = user.getDisplayName().isEmpty() ? "unknown" : user.getDisplayName();

            requestToString = deletedRequestToString.isEmpty() ? this.vacationsHelper.requestToString(request, "without_approve_inf") : deletedRequestToString;

            subject = "Approve for " + requestToString + " from " + userName;
            take = request.getRequestType().equals(UserRequests.RequestTypes.cancel) ? "" : " take";

            message = "Hi, " + userName + ",\n\nWe are Ok with this. User event was added to your account.\n\n";
            message += "Thank you, \n\n";

            message += "I would like to" + take + " " + requestToString + ".\n\n";
            if (request.getDescription() != null) {
                message += request.getDescription() + "\n\n";
            }
            message += "Notice: This message was automatically generated. Please don’t reply on it.\n\n";
            message += "Thank you, \n" + userName;

            return sendMail(to, from, cc, subject, message);
        }

        return false;
    }

    private boolean sendMail(String to, String from, String cc, String subject, String message)
    {
        try {
            Email email = new Email(to);
            email.setFrom(from);
            email.setCc(cc);
            email.setSubject(subject);
            email.setBody(message);
            email.setMimeType("text/plain");

            SingleMailQueueItem smqi = new SingleMailQueueItem(email);
            ComponentAccessor.getMailQueue().addItem(smqi);

            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
}
